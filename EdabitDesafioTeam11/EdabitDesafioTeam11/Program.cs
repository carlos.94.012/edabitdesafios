﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace EdabitDesafioTeam11
{
    public class Program
    {
        static void Main(string[] args)
        {
            EdabitChallenges edabitChallenges = new EdabitChallenges();
            edabitChallenges.PalindromeDescendant(11211230);
            Console.ReadKey();
        }
    }

    public class EdabitChallenges
    {
        #region VeryEasy
        public int Sum(int num1, int num2) => num1 + num2;

        public int MinutesToSecond(int minutes) => minutes * 60;

        public int Addition(int num) => ++num;

        public int SumPolygon(int num)
        {
            if (num < 2)
            {
                throw new ArgumentException("Parameter cannot been less than 2", nameof(num));
            }
            return (num - 2) * 180;
        }

        public string GiveMeSomething(string a) => $"something {a}";


        #endregion VeryEasy

        #region Easy

        public string MonthName(int num)
        {
            if (num < 1 || num > 12)
            {
                throw new ArgumentException("Parameter cannot been less " +
                "than 1 or more than 12", nameof(num));
            }

            return CultureInfo.GetCultureInfo("US").DateTimeFormat.GetMonthName(num);
        }

        public bool IsIdentical(string str)
        {
            char aux = str[0];

            for (int i = 1; i < str.Length; i++)
            {
                if (str[i] != aux)
                {
                    return false;
                }
            }
            return true;
        }

        public int CountClaps(string txt)
        {
            return (from char item in txt
                    where item == 'C'
                    select item).Count();
        }

        public double[] FindLargest(double[][] values) => (from item in values select item.Max()).ToArray();

        public string SevenBoom(int[] arr) => arr.Any(n => n.ToString().Contains('7')) ? "Boom!" : "there is no 7 in the array";

        #endregion Easy

        #region Medium

        public string ReverseCase(string str)
        {
            var result = "";

            for (int i = 0; i < str.Length; i++)
            {
                result += char.IsUpper(str[i]) ? char.ToLower(str[i]) : char.ToUpper(str[i]);
            }

            return result;
        }

        public string Maskify(string str) => new string(str.Select((c, index) => (index <= str.Length - 5) ? '#' : c).ToArray());

        public bool CheckEquality(object a, object b) => a.Equals(b);

        public int PowerRanger(int power, int min, int max)
        {
            int counter = 0;
            int aux = 0;
            double pow = Math.Pow(aux, power);

            while (pow <= max)
            {
                if (pow >= min) counter++;

                aux++;
                pow = Math.Pow(aux, power);
            }

            return counter;

        }

        public byte[] GetUTF8BOM() => Encoding.UTF8.GetPreamble();

        #endregion Medium

        #region Hard

        public List<int> ReversedIntToList(int n)
        {
            var digitos = n.ToString().Select(c => (int)char.GetNumericValue(c)).ToList();
            digitos.Reverse();
            return digitos;
        }

        public int MemeSum(int a, int b)
        {
            var listA = ReversedIntToList(a);
            var listB = ReversedIntToList(b);

            var result = "";

            var longest = listA.Count > listB.Count ? listA.Count : listB.Count;

            for (int i = 0; i < longest; i++)
            {
                var elementA = i >= listA.Count ? 0 : listA[i];
                var elementB = i >= listB.Count ? 0 : listB[i];
                result = (elementA + elementB).ToString() + result;
            }

            return int.Parse(result);
        }

        public int[] TrackRobot(int[] steps)
        {
            int[] direction = { 0, 1 };
            int[] result = { 0, 0 };

            for (int i = 0; i < steps.Length; i++)
            {
                if (i % 2 == 0 && i != 0)
                {
                    direction[0] *= -1;
                    direction[1] *= -1;
                }

                result[0] += direction[0] * steps[i];
                result[1] += direction[1] * steps[i];

                Array.Reverse(direction);
            }
            return result;
        }

        public string WrapAround(string input, int offset)
        {
            int shift = offset % input.Length;
            int indexCut = shift + input.Length;
            int output = indexCut % input.Length;
            return input.Substring(output, input.Length - output) + input.Substring(0, output);
        }

        public string CupSwapping(string[] swaps)
        {
            var position = "B";
            foreach (var item in swaps)
            {
                if (char.IsLower(item[0]) || char.IsLower(item[1]))
                    throw new ArgumentException("The instructions must be in upper case!");

                if (item.Contains(position) && item[0] != item[1])
                    position = item.Trim(position.ToCharArray());
            }
            return position;
        }

        public string ConvertToHex(string inputword)
        {
            var result = "";
            foreach (var c in inputword)
            {
                result += string.Format($"{(int)c:x} ");
                //Convert.ToByte(c).ToString("x2") + " ";
            }
            return result.Remove(result.Length - 1, 1);
        }

        #endregion Hard

        #region VeryHard

        public int License(string me, int agents, string others)
        {
            const int minutes = 20;
            var people = (others + " " + me).Split(' ').ToList();
            people.Sort();
            return ((people.IndexOf(me) / agents) + 1) * minutes;
        }

        public bool IsMagicSquare(int[][] arr)
        {
            if (arr.Length != arr[0].Length)
            {
                return false;
            }

            int width = arr.Length;
            int row = 0;
            int column = 0;
            int principalDiagonal = 0;
            int inverseDiagonal = 0;

            for (int index = 0; index < width * width; index++)
            {
                int x = index / width;
                int y = index % width;
                row += arr[x][y];
                column += arr[y][x];
                principalDiagonal += arr[y][y];
                inverseDiagonal += arr[y][width - 1 - y];
            }

            return row == column && principalDiagonal == inverseDiagonal && row == inverseDiagonal;
        }

        enum CardinalsPoints { NORTH = 0, WEST, SOUTH, EAST }

        public int[] TrackRobot2(string steps)
        {
            int[] position = { 0, 0 };
            int currentDir = (int)CardinalsPoints.EAST;
            Dictionary<int, int[]> directions = new Dictionary<int, int[]>();
            directions.Add((int)CardinalsPoints.NORTH, new int[] { 0, 1 });
            directions.Add((int)CardinalsPoints.WEST, new int[] { -1, 0 });
            directions.Add((int)CardinalsPoints.SOUTH, new int[] { 0, -1 });
            directions.Add((int)CardinalsPoints.EAST, new int[] { 1, 0 });

            foreach (var step in steps.ToList())
            {
                if (step == '>')
                {
                    currentDir = currentDir == 0 ? 3 : currentDir - 1;
                }
                else if (step == '<')
                {
                    currentDir = currentDir == 3 ? 0 : currentDir + 1;
                }
                else
                {
                    position[0] += directions[currentDir][0];
                    position[1] += directions[currentDir][1];
                }
            }
            return position;
        }


        public List<string> Swap(string txt)
        {
            var a = new List<string>();

            if (txt.Length == 1)
            {
                return new List<string>() { txt };
            }
            else
            {
                for (int i = 0; i < txt.Length; i++)
                {
                    var tail = txt.Remove(i, 1);
                    foreach (var item in Swap(tail))
                    {
                        a.Add(txt[i] + item);
                    }
                }
            }
            return a;
        }

        #endregion VeryHard

        #region Expert
        public string Smallest(int n)
        {
            var num = 1;
            for (int i = 2; i <= n; i++)
            {
                var aux = num;
                while (num % i != 0)
                {
                    num += aux;
                }
            }
            return num.ToString();
        }

        public string Permutations(string s)
        {
            var result = "";
            var subList = new List<string>();

            foreach (var item in Swap(s))
            {
                subList.Add(item + " ");
            }

            subList.Sort();

            foreach (var item in subList)
            {
                result += item;
            }
            return result.TrimEnd();
        }

        public bool IsPalindrome(string s) => s.Reverse().SequenceEqual(s);
        public int CharToInt(char c) => int.Parse(c.ToString());

        public bool PalindromeDescendant(int num)
        {
            var stringToNumber = num.ToString();

            if (IsPalindrome(stringToNumber))
            {
                return true;
            }

            while (IsMoreThan2Digits(stringToNumber)
                && IsEven(stringToNumber))
            {
                stringToNumber = num.ToString();
                if (IsPalindrome(stringToNumber))
                {
                    return true;
                }
                num = SumDigits(num);
            }

            return false;
        }

        private static bool IsEven(string stringToNumber) => stringToNumber.Length % 2 < 1;
        private static bool IsMoreThan2Digits(string stringToNumber) => stringToNumber.Length > 2;

        private int SumDigits(int number)
        {
            var stringToNumber = number.ToString();
            var result = "";
            for (int i = 0; i < stringToNumber.Length - 1; i += 2)
            {
                int current = CharToInt(stringToNumber[i]);
                int next = CharToInt(stringToNumber[i + 1]);
                int sum = current + next;
                result += sum.ToString();
            }

            return int.Parse(result);
        }

        #endregion Expert
    }
}
